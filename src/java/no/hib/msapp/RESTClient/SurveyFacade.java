/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.RESTClient;

import java.util.List;
import no.hib.msapp.entities.ConsultationPreperation;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Leif Arne
 */
public class SurveyFacade {

    private final String ALL_SURVEYS = "http://healthservices-consultationpreperation.azurewebsites.net/api/preperation";
    private Client client;
    private WebTarget target;

    public SurveyFacade(String ssn) {
        client = ClientBuilder.newClient();

        target = client.target(ALL_SURVEYS);
        target = target.path(ssn);
    }

    public List<ConsultationPreperation> findAll() {
        return target.path("all").request(MediaType.APPLICATION_JSON).get().readEntity(new GenericType<List<ConsultationPreperation>>() {
        });
    }

    public ConsultationPreperation findNextSurvey() {
        return target.path("next").request(MediaType.APPLICATION_JSON).get().readEntity(ConsultationPreperation.class);
    }

    public void saveSurvey(ConsultationPreperation survey) {
        client.target(ALL_SURVEYS).request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.entity(survey, MediaType.APPLICATION_JSON_TYPE));
    }

}
