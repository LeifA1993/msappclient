/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.RESTClient;

import java.util.List;
import no.hib.msapp.entities.ConsultationPreperation;
import no.hib.msapp.entities.Patient;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;



/**
 *
 * @author Leif Arne
 */
public class PatientFacade {
    private final String ALL_PATIENTS = "http://healthservices-patient.azurewebsites.net/api/patients";
    private Client client;
    private WebTarget target;
    
 
    public PatientFacade() {
        client = ClientBuilder.newClient();       
        target = client.target(ALL_PATIENTS);
    }
    
    public Patient findPatient(int id){      
       Patient p = target.path("" + id).request(MediaType.APPLICATION_JSON).get(Patient.class);
        return p;
        //
       // return patient;
    }
    
    public Patient findPatientBySsn(String Ssn){
        //10111213141
        return target.path(Ssn).request(MediaType.APPLICATION_JSON).get(Patient.class);
    }
    
    public void updatePatient(Patient patient){
        target.path("" + patient.getId()).request(MediaType.APPLICATION_JSON_TYPE).put(Entity.entity(patient, MediaType.APPLICATION_JSON_TYPE));
    }
}
