

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.RESTClient;

import java.util.List;
import no.hib.msapp.entities.ConsultationPreperation;
import no.hib.msapp.entities.Patient;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import no.hib.msapp.entities.Appointment;


/**
 *
 * @author Leif Arne
 */
public class AppointmentFacade {
  private final String ALL_SURVEYS = "http://healthservices-consultationpreperation.azurewebsites.net/api/preperation";
    private Client client;
    private WebTarget target;
    
 
    public AppointmentFacade(Patient p) {
        client = ClientBuilder.newClient();
        
         target = client.target(ALL_SURVEYS);
         target = target.path("" + p.getId());
    }
    
    public ConsultationPreperation findSurvey(int id){      
        return target.path("" + id).request(MediaType.APPLICATION_JSON).get().readEntity(ConsultationPreperation.class);
    }
    
    public ConsultationPreperation findNextSurvey(){
        return target.path("next").request(MediaType.APPLICATION_JSON).get().readEntity(ConsultationPreperation.class);
    }
 

    public void saveSurvey(ConsultationPreperation survey){
     Response r = target.path("" + survey.getConsultationId())
                          .request(MediaType.APPLICATION_JSON_TYPE)
                          .put(Entity.entity(survey, MediaType.APPLICATION_JSON_TYPE));

    }
    
       
}