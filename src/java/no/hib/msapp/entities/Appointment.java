/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/*
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Leif Arne
 */
@XmlRootElement
public class Appointment implements Serializable {

    private static final long serialVersionUID = 1L;
   @XmlElement(name="Id")
    private Long id;
   @XmlElement(name="DateTime")
    private Date dateTime;
    
    @XmlElement(name="ConsultationPreperation")
    private ConsultationPreperation consultationPreperation;
    
    @XmlElement(name="Guid")
    private String guid;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appointment)) {
            return false;
        }
        Appointment other = (Appointment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "no.hib.msapp.entities.Appointment[ id=" + id + " ]";
    }

    /**
     * @return the dateTime
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * @return the consultationPreperation
     */
    public ConsultationPreperation getConsultationPreperation() {
        return consultationPreperation;
    }

    /**
     * @param consultationPreperation the consultationPreperation to set
     */
    public void setConsultationPreperation(ConsultationPreperation consultationPreperation) {
        this.consultationPreperation = consultationPreperation;
    }

    /**
     * @return the guid
     */
    public String getGuid() {
        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }
    
}
