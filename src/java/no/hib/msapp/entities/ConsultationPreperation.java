/*
* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.DateFormat; 
import java.text.SimpleDateFormat;


/*
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Leif Arne
 */

@XmlRootElement
public class ConsultationPreperation implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @XmlElement(name="Id")
    private Long id;
    
    @XmlElement(name="ConsultationTime")
    private Date consultationTime;
    
    @XmlElement(name="NeedForConsultation")
    private String needForConsultation;

    @XmlElement(name="Symptoms")
    private List<Symptom> symptoms;

    @XmlElement(name="HasSideEffects")
    private String hasSideEffects;
    
    @XmlElement(name="NewSideEffectsDegree")
    private String newSideEffectsDegree;
    
    @XmlElement(name="OldSideEffectsDegree")
    private String oldSideEffectsDegree;
    
    @XmlElement(name="SideEffectIsImportant")
    private boolean sideEffectsImportant;
    
    @XmlElement(name="OtherSubjectsList")
    private OtherSubject[] otherSubjects;
    
    @XmlElement(name="OtherSubjectsNote")
    private String otherSubjectsNote;
    
    @XmlElement(name="SymptomListUpdated")
    private String symptomListUpdated;
    
    @XmlElement(name="SideEffectsUpdated")
    private String sideEffectsUpdated;
    
    @XmlElement(name="SideEffectsNote")
    private String sideEffectsNote;
    
    @XmlElement(name="ConsultationId")
    private long consultationId;
    
    @XmlElement(name="Guid")
    private String guid;

    @XmlElement(name="ConsultationGuid")
    private String ConsultationGuid;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsultationPreperation)) {
            return false;
        }
        ConsultationPreperation other = (ConsultationPreperation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "no.hib.msapp.entities.Survey[ id=" + id + " ]";
    }

    /**
     * @return the needForConsultation
     */
    public String getNeedForConsultation() {
        return needForConsultation;
    }

    /**
     * @param needForConsultation the needForConsultation to set
     */
    public void setNeedForConsultation(String needForConsultation) {
        this.needForConsultation = needForConsultation;
    }

    /**
     * @return the symptoms
     */
    public List<Symptom> getSymptoms() {
        return symptoms;
    }

    /**
     * @param symptoms the symptoms to set
     */
    public void setSymptoms(List<Symptom> symptoms) {
        this.symptoms = symptoms;
    }

    /**
     * @return the newSideEffectsDegree
     */
    public String getNewSideEffectsDegree() {
        return newSideEffectsDegree;
    }

    /**
     * @param newSideEffectsDegree the newSideEffectsDegree to set
     */
    public void setNewSideEffectsDegree(String newSideEffectsDegree) {
        this.newSideEffectsDegree = newSideEffectsDegree;
    }

    /**
     * @return the oldSideEffectsDegree
     */
    public String getOldSideEffectsDegree() {
        return oldSideEffectsDegree;
    }

    /**
     * @param oldSideEffectsDegree the oldSideEffectsDegree to set
     */
    public void setOldSideEffectsDegree(String oldSideEffectsDegree) {
        this.oldSideEffectsDegree = oldSideEffectsDegree;
    }

    /**
     * @return the sideEffectsImportant
     */
    public boolean isSideEffectsImportant() {
        return sideEffectsImportant;
    }

    /**
     * @param sideEffectsImportant the sideEffectsImportant to set
     */
    public void setSideEffectsImportant(boolean sideEffectsImportant) {
        this.sideEffectsImportant = sideEffectsImportant;
    }

    /**
     * @return the otherSubjects
     */
    public OtherSubject[] getOtherSubjects() {
        return otherSubjects;
    }

    /**
     * @param otherSubjects the otherSubjects to set
     */
    public void setOtherSubjects(OtherSubject[] otherSubjects) {
        this.otherSubjects = otherSubjects;
    }

    /**
     * @return the otherSubjectsNote
     */
    public String getOtherSubjectsNote() {
        return otherSubjectsNote;
    }

    /**
     * @param otherSubjectsNote the otherSubjectsNote to set
     */
    public void setOtherSubjectsNote(String otherSubjectsNote) {
        this.otherSubjectsNote = otherSubjectsNote;
    }

    /**
     * @return the symptomListUpdated
     */
    public String getSymptomListUpdated() {
        return symptomListUpdated;
    }

    /**
     * @param symptomListUpdated the symptomListUpdated to set
     */
    public void setSymptomListUpdated(String symptomListUpdated) {
        this.symptomListUpdated = symptomListUpdated;
    }

    /**
     * @return the sideEffectsUpdated
     */
    public String getSideEffectsUpdated() {
        return sideEffectsUpdated;
    }

    /**
     * @param sideEffectsUpdated the sideEffectsUpdated to set
     */
    public void setSideEffectsUpdated(String sideEffectsUpdated) {
        this.sideEffectsUpdated = sideEffectsUpdated;
    }

    /**
     * @return the hasSideEffects
     */
    public String getHasSideEffects() {
        return hasSideEffects;
    }

    /**
     * @param hasSideEffects the hasSideEffects to set
     */
    public void setHasSideEffects(String hasSideEffects) {
        this.hasSideEffects = hasSideEffects;
    }

    /**
     * @return the sideEffectsNote
     */
    public String getSideEffectsNote() {
        return sideEffectsNote;
    }

    /**
     * @param sideEffectsNote the sideEffectsNote to set
     */
    public void setSideEffectsNote(String sideEffectsNote) {
        this.sideEffectsNote = sideEffectsNote;
    }

    /**
     * @return the consultationTime
     */
    public String getConsultationTime() {
       DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); 
       return dateFormat.format(consultationTime);
    }
    
    public String getFormattedTime(){
        DateFormat dateFormat = new SimpleDateFormat("dd MMMMMMMMMM YYYY."); 
       return dateFormat.format(consultationTime);
    }
    
    public String getFormattedTime(String format){
      DateFormat dateFormat = new SimpleDateFormat(format); 
       return dateFormat.format(consultationTime);
    }
    
    public Date getConsultationDate(){
        return consultationTime;
    }

    /**
     * @param consultationTime the consultationTime to set
     */
    public void setConsultationTime(Date consultationTime) {
        this.consultationTime = consultationTime;
    }

    /**
     * @return the consultationId
     */
    public long getConsultationId() {
        return consultationId;
    }

    /**
     * @param consultationId the consultationId to set
     */
    public void setConsultationId(long consultationId) {
        this.consultationId = consultationId;
    }

    /**
     * @return the guid
     */
    public String getGuid() {
        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }

    /**
     * @return the ConsultationGuid
     */
    public String getConsultationGuid() {
        return ConsultationGuid;
    }

    /**
     * @param ConsultationGuid the ConsultationGuid to set
     */
    public void setConsultationGuid(String ConsultationGuid) {
        this.ConsultationGuid = ConsultationGuid;
    }

}
