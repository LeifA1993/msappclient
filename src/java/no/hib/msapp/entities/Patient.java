/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
/*
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Leif Arne
 */

@XmlRootElement
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement(name="Id")
    private Long id;
    @XmlElement
    private String Ssn;
    @XmlElement
    private String FirstName;
    @XmlElement
    private String LastName;
    @XmlElement
    private String Sex;
    @XmlElement
    private String Address;
    @XmlElement
    private String Zipcode;
    @XmlElement
    private String City;
    
    public Patient(){
        
    }
  //@XmlElementWrapper
    @XmlElement(name="Consultations")
    private List<Appointment> appointments;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patient)) {
            return false;
        }
        Patient other = (Patient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "no.hib.msapp.entities.Patient[ id=" + id + " ]";
    }

    /**
     * @return the Ssn
     */
    public String getSsn() {
        return Ssn;
    }

    /**
     * @param Ssn the Ssn to set
     */
    public void setSsn(String Ssn) {
        this.Ssn = Ssn;
    }

    /**
     * @return the GivenName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     * @param FirstName the GivenName to set
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     * @return the FamilyName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     * @param LastName the FamilyName to set
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     * @return the Sex
     */
    public String getSex() {
        return Sex;
    }

    /**
     * @param Sex the Sex to set
     */
    public void setSex(String Sex) {
        this.Sex = Sex;
    }

    /**
     * @return the appointments
     */
  
    public List<Appointment> getAppointments() {
        return appointments;
    }

    /**
     * @param appointments the appointments to set
     */
    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    /**
     * @return the Address
     */
    public String getAddress() {
        return Address;
    }

    /**
     * @param Address the Address to set
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     * @return the Zipcode
     */
    public String getZipcode() {
        return Zipcode;
    }

    /**
     * @param Zipcode the Zipcode to set
     */
    public void setZipcode(String Zipcode) {
        this.Zipcode = Zipcode;
    }

    /**
     * @return the City
     */
    public String getCity() {
        return City;
    }

    /**
     * @param City the City to set
     */
    public void setCity(String City) {
        this.City = City;
    }

}
