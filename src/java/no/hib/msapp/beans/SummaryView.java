/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.beans;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import no.hib.msapp.entities.ConsultationPreperation;
import no.hib.msapp.entities.Symptom;

/**
 *
 * @author Leif Arne
 */
@ManagedBean
public class SummaryView {

    /**
     * Creates a new instance of SummaryView
     */
    private ConsultationPreperation survey = new ConsultationPreperation();
    
     @PostConstruct
    public void initBean() {
        setSurvey((ConsultationPreperation) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("survey"));
    }
    
    public SummaryView() {
    }

    /**
     * @return the survey
     */
    public ConsultationPreperation getSurvey() {
        return survey;
    }

    public String goBack(){
        return "consultationHistory.xhtml";
    }
    
    /**
     * @param survey the survey to set
     */
    public void setSurvey(ConsultationPreperation survey) {
        this.survey = survey;
    }
    
}
