/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import no.hib.msapp.RESTClient.SurveyFacade;
import no.hib.msapp.entities.ConsultationPreperation;
import java.util.Locale;

/**
 *
 * @author Leif Arne
 */
@Named(value = "nextControlView")
@Dependent
public class NextControlView {
    private Date meetingDate;
    private SurveyFacade surveyFacade;
    private Date answerDate;
    private DateFormat dateFormat;
    
    
    /**
     * Creates a new instance of NextControlView
     */
    public NextControlView() {
         String ssn = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Ssn");
        surveyFacade = new SurveyFacade(ssn);
        meetingDate = surveyFacade.findNextSurvey().getConsultationDate();
        
        Calendar cal = Calendar.getInstance();
cal.setTime(meetingDate);
cal.add(Calendar.DATE, -7);
    answerDate = cal.getTime();
    Locale local = new Locale("no");
    dateFormat = new SimpleDateFormat("dd MMMMMMMMMM YYYY.", local); 
    }
    
    public String getStage(){
        long meetingTimeInMilliseconds = meetingDate.getTime();
        long now = System.currentTimeMillis();
        
        long difference = meetingTimeInMilliseconds - now;
        
        
        //Less than 1 week
        if (difference < 1000 * 60 * 60 * 24 * 7){
			return "dateExpired";
		}
	//More than 3 weeks	
		else if (difference > 3000 * 60 * 60 * 24 * 7 ) {
			return "informAboutSurvey";
		}
		
                // between 1 and 3 weeks
		else {
			return "displaySurveyOption";
		}	
    }

    /**
     * @return the meetingDate
     */
    public String getMeetingDate() {
        return dateFormat.format(meetingDate);
    }

    /**
     * @return the answerDate
     */
    public String getAnswerDate() {
        return dateFormat.format(answerDate);
    }
    
}
