/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.beans;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import java.util.List;
import javax.faces.context.FacesContext;
import no.hib.msapp.RESTClient.PatientFacade;
import no.hib.msapp.RESTClient.SurveyFacade;
import no.hib.msapp.entities.ConsultationPreperation;

/**
 *
 * @author Leif Arne
 */
@Named(value = "surveyListView")
@Dependent
public class SurveyListView {

    /**
     * Creates a new instance of SurveyListView
     */
    
    private List<ConsultationPreperation> surveys;
    private PatientFacade patientFacade = new PatientFacade();
    private SurveyFacade surveyFacade;
    private List<ConsultationPreperation> consultations; 
    public SurveyListView() {
     
        String ssn = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Ssn");

        surveyFacade = new SurveyFacade(ssn);
        consultations = surveyFacade.findAll();
        
    }

    /**
     * @return the surveys
     */
    public List<ConsultationPreperation> getSurveys() {
        return surveys;
    }

      public String viewSurvey(ConsultationPreperation survey) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("survey", survey);
        return "summary.xhtml";
    }
    
    /**
     * @param surveys the surveys to set
     */
    public void setSurveys(List<ConsultationPreperation> surveys) {
        this.surveys = surveys;
    }

    /**
     * @return the consultations
     */
    public List<ConsultationPreperation> getConsultations() {
        return consultations;
    }

    /**
     * @param consultations the consultations to set
     */
    public void setConsultations(List<ConsultationPreperation> consultations) {
        this.consultations = consultations;
    }
    
}
