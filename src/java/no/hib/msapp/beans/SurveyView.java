/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.hib.msapp.beans;

import java.io.Serializable;
import no.hib.msapp.entities.Symptom;
import javax.inject.Named;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import no.hib.msapp.entities.ConsultationPreperation;
import no.hib.msapp.RESTClient.SurveyFacade;

/**
 *
 * @author Leif Arne
 */
@Named
@ConversationScoped
public class SurveyView implements Serializable {

    
    private int step = 1;
    private ConsultationPreperation survey;
    private SurveyFacade surveyFacade;
    private final int MAX_STEPS = 7;

    @Inject
    private Conversation conversation;

    public SurveyView() {
      String ssn = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Ssn");
        surveyFacade = new SurveyFacade(ssn);
        survey = surveyFacade.findNextSurvey();
    }

    public void initConversation() {
        if (!FacesContext.getCurrentInstance().isPostback()
                && conversation.isTransient()) {

            conversation.begin();
        }
    }

    public String endConversation() {
        if (!conversation.isTransient()) {
            conversation.end();
        }
        return "myPage?faces-redirect=true";
    }

    public Conversation getConversation() {
        return conversation;
    }

  

    public String viewSymptom(Symptom symptom) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("symptom", symptom);
        return "symptomTemplate.xhtml";
    }

    public void incrementStep() {
        setStep(getStep() + 1);
        surveyFacade.saveSurvey(survey);
    }

    public void decrementStep() {
        setStep(getStep() - 1);
        surveyFacade.saveSurvey(survey);
    }


    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }
    
    public String goToConfirmation(){
        surveyFacade.saveSurvey(survey);
        return "confirmation.xhtml";
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the survey
     */
    public ConsultationPreperation getSurvey() {
        return survey;
    }

    /**
     * @param survey the survey to set
     */
    public void setSurvey(ConsultationPreperation survey) {
        this.survey = survey;
    }

    /**
     * @return the MAX_STEPS
     */
    public int getMAX_STEPS() {
        return MAX_STEPS;
    }

 

}
